27.03.2019 �������� ���������� Lightning Network � ������� �� ����������
Learning Lightning Network technology and how to use it
31.03.2019 ������ �������� ���������� Lightning Network � ���������
Analysis of Lightning Network and blockchain technology development
07.04.2019 ������ ���������� ���������� ��������, � ��������� � ���������� ������� (�������� �.�., ������������ �.�. �������� ���������� � ���������� �����)
Analysis of the use of blockchain technology, in particular in the financial sector (Andreeva O. V., fatkhutdinova R. A. Blockchain technology in the financial sector)
14.04.2019 ����� � ������ �������� ������������� ���������� � ��������������� �������� � ��� ����������� (������ �.�. �������� ��� ������������� ���������� ������������������� ��������������)
Search and analysis of examples of the use of blockchains and planned projects with its application (Tsuryupa E. O. Blockchain as a revolutionary technology of decentralized interaction)
17.04.2019 ������ � ������ �������� ���������� �������� � ������� �����������
Evaluation and analysis of blockchain technology development in the context of cryptocurrencies
21.04.2019 ������ ��������� ������� � ����� �������� ��������� (�������� �.�. ����������� ��������� � ������ � �� �������: �������� �������������. 2-� ���.)
Analysis of the legal aspect in the field of digital Commerce (Savelyev A. I. E-Commerce in Russia and abroad: legal regulation. 2-e Izd.)
28.04.2019 ����������� �������� ��������� ������������� ���������� �������
Continued study of legal regulation of blockchain technology
01.05.2019 ����������� � ���������� ����� ��������� ������
Merging and sorting previously done work
05.05.2019 ������������ ��������� ������, ����� �������� ���������� � ������� �������
Formation of the work structure, search for the latest information in scientific articles
12.05.2019 ���������� ���� ������� �� ��������������� �������, ����� ����� ���������� ����������
Extension of the analysis topic to the distributed registry, search for new sources of information
17.05.2019 �������� ����������� ���������� �������������� �������� � ��������� ����� �� ����������
A study of the benefits of technology distributed registries and possible areas of their application
19.05.2019 �������� ������, �������������� ����� ����������� �����������
Make edits, edit the choice of future direction
23.05.2019 ������ ������ � �������, ������� ������ �������������� ��������
Analysis of risks and challenges posed by distributed registries
26.05.2019 ������ ����������� � ����� ��� ����� �������� � ���������� ���������� �������������� ��������
Analysis of Russian and CIS experience in the study and application of the technologies of distributed registries
29.05.2019 �������� �������� ����� �������� � ���������� ���������� �������������� ��������
Study of world experience in the development and application of distributed registry technology
02.06.2019 �������� ���������������� ������������� �������������� ��������� � ���, ������, ��������� � ���������
Study of legal regulation of distributed registries in the USA, Canada, Switzerland and Singapore
02.06.2019 �������������� � �������������� ������
Formatting and editing work